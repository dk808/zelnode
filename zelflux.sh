#!/bin/bash

COIN_NAME=zelcash
USERNAME="$(whoami)"

#Zelflux ports
ZELFRONTPORT=16126
LOCPORT=16127
ZELNODEPORT=16128
MDBPORT=27017

#color codes
RED='\033[1;31m'
YELLOW='\033[1;33m'
BLUE="\\033[38;5;27m"
SEA="\\033[38;5;49m"
GREEN='\033[1;32m'
CYAN='\033[1;36m'
NC='\033[0m'

#emoji codes
CHECK_MARK="${GREEN}\xE2\x9C\x94${NC}"
X_MARK="${RED}\xE2\x9D\x8C${NC}"
PIN="${RED}\xF0\x9F\x93\x8C${NC}"

#end of required details
#

function ip_confirm() {
	echo -e "${YELLOW}Detecting IP address being used...${NC}" && sleep 1
	WANIP=$(wget http://ipecho.net/plain -O - -q)
	whiptail --yesno "Detected IP address is $WANIP is this correct?" 8 60
	if [ $? = 1 ]; then
		WANIP=$(whiptail --inputbox "        Enter IP address" 8 36 3>&1 1>&2 2>&3)
	fi
}

function spinning_timer() {
	animation=( ⠋ ⠙ ⠹ ⠸ ⠼ ⠴ ⠦ ⠧ ⠇ ⠏ )
	end=$((SECONDS+$NUM))
	while [ $SECONDS -lt $end ];
	do
		for i in ${animation[@]};
		do
			echo -ne "${RED}\r$i ${CYAN}${MSG1}${NC}"
			sleep 0.1
		done
	done
	echo -e "${MSG2}"
}

function kill_sessions() {
	echo -e "${YELLOW}Detecting sessions please remove any that is running Zelflux...${NC}" && sleep 5
	tmux ls | grep : | cut -d "" -f1 | awk '{print substr($1, 0, length($1)-1)}' | tee tempfile > /dev/null 2>&1
	for line in $(cat tempfile)
		do
			whiptail --yesno "Would you like to kill session ${line}?" 8 43
			if [ $? = 0 ]; then
				tmux kill-sess -t ${line}
			fi
		done
		rm tempfile
}

function install_zelflux() {
		echo -e "${YELLOW}Detect OS version to install Mongodb, Nodejs, and updating firewall...${NC}"
		sudo ufw allow $ZELFRONTPORT/tcp
		sudo ufw allow $LOCPORT/tcp
		sudo ufw allow $ZELNODEPORT/tcp
		sudo ufw allow $MDBPORT/tcp
		if [[ $(lsb_release -r) = *16.04* ]]; then
			wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
			echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
			sudo apt-get update
			sudo apt-get install mongodb-org -y
			sudo service mongod start
			install_nodejs
		elif [[ $(lsb_release -r) = *18.04* ]]; then
			wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
			echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
			sudo apt-get update
			sudo apt-get install mongodb-org -y
			sudo service mongod start
			install_nodejs
		elif [[ $(lsb_release -d) = *Debian* ]] && [[ $(lsb_release -d) = *9* ]]; then
			wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
			echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.2 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
			sudo apt-get update
			sudo apt-get install mongodb-org -y
			sudo service mongod start
			install_nodejs
		fi
		sleep 2
	}

function install_nodejs() {
	if ! node -v > /dev/null 2>&1; then
		curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh | bash
		. ~/.profile
		nvm install --lts
	else
		echo -e "${YELLOW}Nodejs already installed will skip installing it.${NC}"
	fi
	if [ -d "./zelflux" ]; then
		sudo rm -rf zelflux
	fi
	kill_sessions
	ZELID=$(whiptail --inputbox "Enter your ZelID found in the Zelcore+/Apps section of your Zelcore" 8 71 3>&1 1>&2 2>&3)
	TMUX=$(whiptail --inputbox "Enter a name for your tmux session to run Zelflux" 8 53 3>&1 1>&2 2>&3)
	if ! tmux ls | grep -q $TMUX; then
		tmux new-session -d -s $TMUX
		tmux send-keys 'git clone https://dk808@bitbucket.org/dk808/zelflux.git && cd zelflux && npm start' C-m
		NUM='300'
		MSG1="Cloning and installing Zelflux. Please be patient this will take 5 min..."
		MSG2="${CHECK_MARK}${CHECK_MARK}${CHECK_MARK}${GREEN} installation has completed${NC}"
		echo && spinning_timer
		sleep 2
		tmux send-keys "$WANIP" C-m
		sleep 2
		tmux send-keys "$ZELID" C-m
		sleep 1
		SESSION_NAME="$TMUX"
	else
		tmux new-session -d -s ${COIN_NAME^}
		tmux send-keys 'git clone https://dk808@bitbucket.org/dk808/zelflux.git && cd zelflux && npm start' C-m
		NUM='300'
		MSG1="Cloning and installing Zelflux. Please be patient this will take 5 min..."
		MSG2="${CHECK_MARK}${CHECK_MARK}${CHECK_MARK}${GREEN} installation has completed${NC}"
		echo && spinning_timer
		sleep 2
		tmux send-keys "$WANIP" C-m
		sleep 2
		tmux send-keys "$ZELID" C-m
		sleep 1
		SESSION_NAME="${COIN_NAME^}"
	fi
}

function check() {
	echo && echo
	echo -e "${YELLOW}Running through some checks...${NC}"
	if pgrep zelcashd > /dev/null; then
		echo -e "${CHECK_MARK} ${CYAN}${COIN_NAME^} daemon is installed and running${NC}" && sleep 1
	else
		echo -e "${X_MARK} ${CYAN}${COIN_NAME^} daemon is not running${NC}" && sleep 1
	fi
	if [ -d "/home/$USERNAME/.zcash-params" ]; then
		echo -e "${CHECK_MARK} ${CYAN}zkSNARK params installed${NC}" && sleep 1
	else
		echo -e "${X_MARK} ${CYAN}zkSNARK params not installed${NC}" && sleep 1
	fi
	if pgrep mongod > /dev/null; then
		echo -e "${CHECK_MARK} ${CYAN}Mongodb is installed and running${NC}" && sleep 1
	else
		echo -e "${X_MARK} ${CYAN}Mongodb is not running or you have opted out of installing Zelflux${NC}" && sleep 1
	fi
	if node -v > /dev/null 2>&1; then
		echo -e "${CHECK_MARK} ${CYAN}Nodejs installed${NC}" && sleep 1
	else
		echo -e "${X_MARK} ${CYAN}Nodejs not installed or you have opted out of installing Zelflux${NC}" && sleep 1
	fi
	if [ -d "/home/$USERNAME/zelflux" ]; then
		echo -e "${CHECK_MARK} ${CYAN}Zelflux installed${NC}" && sleep 1
	else
		echo -e "${X_MARK} ${CYAN}Zelflux not installed ${NC}" && sleep 1
	fi
	echo && echo
}

function display_banner() {
		printf "${BLUE}"
		figlet -t -k "ZELFLUX"
		printf "${NC}"
		echo -e "${YELLOW}================================================================================================================="
		echo -e "${YELLOW}   Your tmux session running Zelflux is named ${SESSION_NAME}${NC}"
		echo -e "${PIN} ${CYAN}To attach to zelflux session enter: ${SEA}tmux a -t ${SESSION_NAME}${NC}"
		echo -e "${PIN} ${CYAN}To detach zelflux session enter: ${SEA}Ctrl+b, d${NC}"
		echo -e "${PIN} ${CYAN}To kill zelflux session enter: ${SEA}tmux kill-session -t ${SESSION_NAME}${NC}"
		echo
		echo -e "${PIN} ${CYAN}To access your frontend to Zelflux enter this in as your url: ${SEA}${WANIP}:${ZELFRONTPORT}${NC}"
		echo -e "${YELLOW}=================================================================================================================${NC}"
}

ip_confirm
install_zelflux
check
display_banner
