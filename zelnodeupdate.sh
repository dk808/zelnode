#!/bin/bash

#wallet information
COIN_NAME='zelcash'
COIN_DAEMON='zelcashd'
COIN_CLI='zelcash-cli'
COIN_PATH='/usr/local/bin'
USERNAME=$(whoami)

#color codes
RED='\033[1;31m'
YELLOW='\033[1;33m'
GREEN='\033[1;32m'
CYAN='\033[1;36m'
NC='\033[0m'

#emoji code
CHECK_MARK="${GREEN}\xE2\x9C\x94${NC}"

clear
echo -e "${YELLOW}==========================================================="
echo -e " ZelNode Update"
echo -e "===========================================================${NC}"
echo -e "${CYAN}Nov 2019, by Alttank Army, dk808, Goose-Tech & Skyslayer${NC}"
echo
echo -e "${CYAN}ZelNode update starting, press [CTRL-C] to cancel.${NC}"
sleep 4
echo
#check for correct user
if [ "$USERNAME" = "root" ]; then
    echo -e "${CYAN}You are currently logged in as ${GREEN}root${CYAN}, please switch to the user you created your Zelnode with.${NC}"
    exit
fi

#Spinning animation to use as a timer with message
function spinning_timer() {
    animation=( ⠋ ⠙ ⠹ ⠸ ⠼ ⠴ ⠦ ⠧ ⠇ ⠏ )
    end=$((SECONDS+$NUM))
    while [ $SECONDS -lt $end ];
    do
        for i in ${animation[@]};
        do
            echo -ne "${RED}\r$i ${CYAN}${MSG1}${NC}"
            sleep 0.1
        done
    done
    echo -e "${MSG2}"
}

#Closing zelcash daemon
echo -e "${CYAN}Stopping daemon...${NC}"
sudo systemctl stop $COIN_NAME > /dev/null 2>&1 && sleep 3
sudo $COIN_CLI stop > /dev/null 2>&1 && sleep 3
sudo killall $COIN_DAEMON > /dev/null 2>&1

#Updating zelcash package
echo
echo -e "${CYAN}Upgrading ${COIN_NAME^} package...${NC}"
sudo apt-get update
sudo apt-get install --only-upgrade $COIN_NAME -y
sudo chmod 755 ${COIN_PATH}/${COIN_NAME}*
$COIN_DAEMON > /dev/null 2>&1
echo
NUM=60
MSG1="Starting up daemon and fetching info please wait patiently this should take about a min..."
MSG2="${CHECK_MARK}${CHECK_MARK}${CHECK_MARK}"
spinning_timer
echo
$COIN_CLI getinfo
